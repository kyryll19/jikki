package dao;

import entity.Entry;

/**
 * Created by kissinger on 21.06.15.
 */
public interface EntryDao {
    Long save(Entry user);
}
