package dao;

import entity.Entry;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by kissinger on 21.06.15.
 */
@Repository
@Transactional
public class EntryHibDaoImpl implements EntryDao {
    @Autowired
    SessionFactory sessionFactory;

    @Override
    public Long save(Entry entry) {
        Session session = sessionFactory.getCurrentSession();
        return (Long) session.save(entry);
    }
}
