package entity;

import marshaller.DateAdapter;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.Date;

/**
 * Created by kissinger on 21.06.15.
 */
@Entity
@Table
@XmlRootElement
public class Entry  implements Cloneable {
    @Id
    @GeneratedValue
    Long id;

    String content;

    Date date;

    @XmlElement
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @XmlElement
    @XmlJavaTypeAdapter(DateAdapter.class)
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Entry(String content, Date date) {
        this.content = content;
        this.date = date;
    }

    public Entry() {
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
