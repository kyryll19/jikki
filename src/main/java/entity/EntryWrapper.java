package entity;

import org.springframework.stereotype.Component;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kyryll on 29.06.2015.
 */
@XmlRootElement(name = "entries")
@Component
public class EntryWrapper implements Serializable{
    private List<Entry> entries = new ArrayList<>();

    public void add(Entry entry) {
        entries.add(entry);
    }
    @XmlElement(name = "entry")
    public List<Entry> getEntries() {
        return entries;
    }

    public void setEntries(List<Entry> entries) {
        this.entries = entries;
    }
}
