package entity;


import javax.persistence.*;

/**
 * Created by Kyryll on 01.07.2015.
 */
@Entity
@Table(name = "Pupibl")
@AttributeOverrides({@AttributeOverride(name = "name1", column = @Column(name = "name"))})
public class Pupibl extends User {
    @Column(name = "nick")
    String nickname;

    public Pupibl(String name) {
        super(name);
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
}
