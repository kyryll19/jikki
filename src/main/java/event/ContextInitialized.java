package event;

import entity.Entry;
import entity.EntryWrapper;
import marshaller.JAXBMarshaller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.ParseException;
import java.util.Date;

/**
 * Created by Kyryll on 29.06.2015.
 */
@Component
public class ContextInitialized implements ApplicationListener<ContextRefreshedEvent> {
    @Autowired
    @Qualifier("directory")
    String directory;

    @Autowired
    @Qualifier("entriesInFile")
    Integer entriesInFile;

    @Autowired
    @Qualifier("filesCount")
    Integer filesCount;

    @Autowired
    JAXBMarshaller marshaller;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        try {
            clearDirectory();
            for (int i = 0; i < filesCount; i++) {
                initEntriesInFile(String.valueOf(i));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
    }

    private void initEntriesInFile(String filename) throws IOException, ParseException, CloneNotSupportedException {
        EntryWrapper wrapper = fillEntries();
        String filePath = directory + "\\" + filename + ".xml";
        Files.write(Paths.get(filePath), marshaller.marhall(wrapper).getBytes(), StandardOpenOption.CREATE,
                StandardOpenOption.TRUNCATE_EXISTING);
    }

    private EntryWrapper fillEntries() throws CloneNotSupportedException {
        EntryWrapper wrapper = new EntryWrapper();
        Entry entry = new Entry("aaa", new Date());
        for (int i = 0; i < entriesInFile; i++) {
            wrapper.add((Entry) entry.clone());
        }
        return wrapper;
    }

    private void clearDirectory() {
        File dir = new File(directory);
        for (File file : dir.listFiles()) {
            file.delete();
        }
    }
}
