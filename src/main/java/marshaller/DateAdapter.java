package marshaller;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Kyryll on 29.06.2015.
 */
public class DateAdapter extends XmlAdapter<String, Date> {
    private DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    public Date unmarshal(String v) throws Exception {
        return formatter.parse(v);
    }

    @Override
    public String marshal(Date v) throws Exception {
        return formatter.format(v);
    }
}
