package marshaller;

import entity.EntryWrapper;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.ByteArrayOutputStream;
import java.io.File;

/**
 * Created by Kyryll on 29.06.2015.
 */
public class JAXBMarshaller {
    public EntryWrapper unmarshall(File xmlFile) {
        JAXBContext jaxbContext = null;
        EntryWrapper entries = null;
        try {
            jaxbContext = JAXBContext.newInstance(EntryWrapper.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            entries = (EntryWrapper) jaxbUnmarshaller.unmarshal(xmlFile);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return entries;
    }

    public String marhall(EntryWrapper wrapper) {
        ByteArrayOutputStream xmlBytes = new ByteArrayOutputStream();
        JAXBContext jaxbContext = null;
        try {
            jaxbContext = JAXBContext.newInstance(EntryWrapper.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            jaxbMarshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
            jaxbMarshaller.marshal(wrapper, xmlBytes);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return xmlBytes.toString();
    }
}
