package monitor;

import dao.EntryDao;
import entity.Entry;
import entity.EntryWrapper;
import marshaller.JAXBMarshaller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kissinger on 21.06.15.
 */
@Component(value = "monitor")
public class Monitor {
    @Autowired
    String directory;
    @Autowired
    EntryDao entryDao;
    @Autowired
    JAXBMarshaller marshaller;

    public List<File> monitor() {
        List<File> filesForProduction = new ArrayList<>();
        try (DirectoryStream<Path> paths = Files.newDirectoryStream(Paths.get(directory))) {
            for (Path path : paths) {
                if (path.toFile().getPath().endsWith(".xml") && checkStructure(path.toFile())) {
                    EntryWrapper entries = marshaller.unmarshall(path.toFile());
                    for (Entry entry : entries.getEntries()) {
                        entryDao.save(entry);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return filesForProduction;
    }

    private boolean checkStructure(File file) throws MalformedURLException, SAXException, URISyntaxException {
        URL schemaFile = getClass().getClassLoader().getResource("entry.xsd").toURI().toURL();
        Source xmlFile = new StreamSource(file);
        SchemaFactory schemaFactory = SchemaFactory
                .newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema = schemaFactory.newSchema(schemaFile);
        Validator validator = schema.newValidator();
        try {
            validator.validate(xmlFile);
            return true;
        } catch (IOException e) {
            return false;
        }
    }


    public static void main(String[] args) {
        Monitor monitor = (Monitor) new ClassPathXmlApplicationContext("applicationContext.xml")
                .getBean("monitor");
        monitor.monitor();
    }
}
